﻿using ApiRFC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace ApiRFC.Servicio
{
    public class RFCServicio
    {
        Response response;
        public RFCServicio() {
            response = new Response();
        }
        public Response validateCFDI(String rfc, String xml) {
            Error error = new Error();

            XDocument cfdi = XDocument.Parse(xml);
                        
            var version = cfdi.Root.Attribute("Version").Value;
            var subtotal = cfdi.Root.Attribute("SubTotal").Value; 
            var rfcEmisor = cfdi.Root.Elements().Where(w => w.Name.LocalName.Equals("Emisor")).Attributes("Rfc").FirstOrDefault().Value;

            var suma = cfdi.Root.Elements().Where(w => w.Name.LocalName == "Conceptos").Elements().Where(w => w.Name.LocalName == "Concepto").Select(s => new { improte = decimal.Parse(s.Attribute("Importe").Value) }).Sum(s => s.improte);

            if (rfcEmisor != rfc)
            {
                response.isvalid = false;
                response.statusCode = (int)HttpStatusCode.BadRequest;
                response.errores.Add(new Error() { campo = "rfc", mensaje = "los rfc no concuerdan" });
            }

            if (version != "3.3") {
                response.isvalid = false;
                response.statusCode = (int)HttpStatusCode.BadRequest;
                response.errores.Add(new Error() { campo = "version", mensaje = "la version no concuerda" });
            }

            if (suma != decimal.Parse(subtotal))
            {
                response.isvalid = false;
                response.statusCode = (int)HttpStatusCode.BadRequest;
                response.errores.Add(new Error() { campo = "SubTotal", mensaje = "el SubTotal no concuerda con las cantidades de los conceptos" });
            }

            return response;
        }
    }
}