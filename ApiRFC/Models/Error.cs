﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiRFC.Models
{
    public class Error
    {
        public String mensaje { get; set; }
        public String campo { get; set; }
    }
}