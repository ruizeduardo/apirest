﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ApiRFC.Models
{
    public class Response
    {
        public Response() {
            errores = new List<Error>();
            isvalid = true;
            statusCode = (int)HttpStatusCode.OK;
        }
        public bool isvalid { get; set; }
        public int statusCode { get; set; }

        public List<Error> errores  { get; set; }
    }
}