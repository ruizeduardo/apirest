﻿using ApiRFC.Models;
using ApiRFC.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRFC.Controllers
{
    [RoutePrefix("api/rfc")]
    public class RFCController : ApiController
    {
        // GET: api/RFC
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/RFC/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/RFC
        [HttpPost]
        [Route("{rfc}", Name = "leer rfc")]
        public Response Post(string rfc, [FromBody]cfdi xml)
        {
            RFCServicio servicio = new RFCServicio(); 
           
            return servicio.validateCFDI(rfc, xml.xml);

        }

        // PUT: api/RFC/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/RFC/5
        public void Delete(int id)
        {
        }
    }
}
